@echo off
call _setup_env.cmd
python -u ..\cegui-sdkbuilder\build_cegui_windows.py --config-file C:\cegui-build\config.json --boost-include-dir C:\local\boost_1_55_0 --boost-library-dir C:\local\boost_1_55_0\lib32-msvc-9.0 %*  2>&1 | tee -a artifacts/logs/build_cegui.log
cd ..