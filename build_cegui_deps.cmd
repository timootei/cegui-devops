@echo off
call _setup_env.cmd
python -u ..\cegui-sdkbuilder\build_cegui_dependencies_windows.py --config-file C:\cegui-build\config.json %*  2>&1 | tee -a artifacts/logs/build_cegui_deps.log
cd ..