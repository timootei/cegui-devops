@echo off
echo *** CEGUI BUILD BOOTSTRAPPER
mkdir workspace\artifacts\logs
echo *** Updating repo
start /w cmd /c "update_builder_repo.cmd 2>&1 | tee -a workspace/artifacts/logs/update_builder_repo.log"
echo *** Building CEGUI Deps
start /w cmd /c "build_cegui_deps.cmd"
echo *** Building CEGUI
start /w cmd /c "build_cegui.cmd"