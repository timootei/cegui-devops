@echo off
echo *** Updating the CEGUI SDK Builder repo...
if not exist cegui-sdkbuilder hg clone https://bitbucket.org/cegui/cegui-sdkbuilder
cd cegui-sdkbuilder
hg pull
hg update -C default
echo *** Done.